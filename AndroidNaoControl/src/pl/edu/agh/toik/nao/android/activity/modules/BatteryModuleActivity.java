package pl.edu.agh.toik.nao.android.activity.modules;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.activity.AbstractActivity;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.task.InvokeTask;
import pl.edu.agh.toik.nao.android.task.interfaces.CommandDelegate;
import android.os.Bundle;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.TextView;

public class BatteryModuleActivity extends AbstractActivity implements CommandDelegate{

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_battery);
        TextView label = ((TextView)findViewById(R.id.serverIP1));
        label.setText(label.getText().toString()+"  "+CommunicationManager.getInstance().getInfoString());
    }
    
    public void refreshButtonClicked(View v){
		InvokeTask it =new InvokeTask();
		it.setModule("BatteryModule");
		it.setParams("");
		it.setVariant("");
		it.setDelegate(this);
		it.execute();
    }

	@Override
	public void onCommandExecuted(final String result) {
		
		runOnUiThread(new Runnable() {
		     @Override
		     public void run() {

		 		((TextView)findViewById(R.id.batteryPercentage)).setText(result+"%");
				((ProgressBar)findViewById(R.id.batteryBar)).setMax(100);
				((ProgressBar)findViewById(R.id.batteryBar)).setProgress(Integer.valueOf(result));

		    }
		});

	}
}
