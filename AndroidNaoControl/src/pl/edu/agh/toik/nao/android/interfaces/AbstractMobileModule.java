package pl.edu.agh.toik.nao.android.interfaces;

import android.support.v4.app.Fragment;

public abstract class AbstractMobileModule {

	private Fragment fragment;
	
	private String moduleName;
	
	public abstract void onResult(String result);

	public Fragment getFragment() {
		return fragment;
	}

	public void setFragment(Fragment fragment) {
		this.fragment = fragment;
	}

	public String getModuleName() {
		return moduleName;
	}

	public void setModuleName(String moduleName) {
		this.moduleName = moduleName;
	}
	
	
}
