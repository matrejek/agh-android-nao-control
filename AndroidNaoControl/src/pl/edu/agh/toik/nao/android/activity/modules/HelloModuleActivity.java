package pl.edu.agh.toik.nao.android.activity.modules;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.activity.AbstractActivity;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.task.InvokeTask;
import pl.edu.agh.toik.nao.android.task.interfaces.CommandDelegate;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;



public class HelloModuleActivity extends AbstractActivity  implements CommandDelegate {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hello);
        TextView label = ((TextView)findViewById(R.id.serverIP1));
        label.setText(label.getText().toString()+"  "+CommunicationManager.getInstance().getInfoString());
    }
    
    public void sendButtonClicked(View v){
		InvokeTask it = new InvokeTask();
		it.setModule("HelloModule");
		it.setParams( ((EditText)findViewById(R.id.nameField)).getText().toString() );
		it.setVariant("");
		it.setDelegate(this);
		it.execute();
    }

	@Override
	public void onCommandExecuted(String result) {
		
	}
}
