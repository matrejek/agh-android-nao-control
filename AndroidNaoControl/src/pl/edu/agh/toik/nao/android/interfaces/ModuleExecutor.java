package pl.edu.agh.toik.nao.android.interfaces;

public interface ModuleExecutor {

	public void execute(String moduleName, String parameters, AbstractMobileModule callbacks);
	
	public ModuleExecutor getInstance();
	
}
