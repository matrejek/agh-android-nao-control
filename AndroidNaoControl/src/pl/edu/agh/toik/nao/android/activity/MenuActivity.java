package pl.edu.agh.toik.nao.android.activity;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import android.os.Bundle;
import android.widget.TextView;



public class MenuActivity extends AbstractActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_menu);
        TextView label = ((TextView)findViewById(R.id.serverIP1));
        label.setText(label.getText().toString()+"  "+CommunicationManager.getInstance().getInfoString());
    }
}
