package pl.edu.agh.toik.nao.android.task.interfaces;

public interface CommandDelegate {

	
	public void onCommandExecuted(String result);
	
	
}
