package pl.edu.agh.toik.nao.android.activity;

import java.util.regex.Pattern;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import pl.edu.agh.toik.nao.android.fragments.ConnectFragment;
import pl.edu.agh.toik.nao.android.fragments.SelectionFragment;
import pl.edu.agh.toik.nao.android.task.TestTask;
import pl.edu.agh.toik.nao.android.task.interfaces.TestDelegate;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

public class ConnectActivity extends AbstractActivity implements TestDelegate{
	
	ConnectFragment connectFragment;

	private String serverIP;
	private String serverPort;
	
	private Pattern pattern;

	private CommunicationManager communicationManager;
	
	private static final String IPADDRESS_PATTERN = 
		"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
		"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
 
	
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_connect);

        connectFragment = new ConnectFragment();
        if (savedInstanceState == null) {
            getFragmentManager().beginTransaction()
                    .add(R.id.container, connectFragment)
                    .commit();
        }
        
        pattern = Pattern.compile(IPADDRESS_PATTERN);
    }
    
    public void connectButtonClicked(View v){

    	EditText addressEdit = (EditText) connectFragment.getView().findViewById(R.id.serverAddressEdit);
    	EditText portEdit = (EditText) connectFragment.getView().findViewById(R.id.serverPortEdit);
    	
    	serverIP = addressEdit.getText().toString();
    	serverPort = portEdit.getText().toString();
    	
    	String message = "";
    	
    	if(serverIP.equalsIgnoreCase("") || serverPort.equalsIgnoreCase("")){

    		message = "Please fill all the fields in order to continue.";
    		
    	}else if ( !pattern.matcher(serverIP).matches()){
    		
    		message = "Please provide vaild IP address in order to continue.";
    	}else{
    		communicationManager = CommunicationManager.getInstance();
    		communicationManager.init(serverIP, Integer.parseInt(serverPort));
            
			TestTask tt =new TestTask();
			tt.setDelegate(this);
			tt.execute();

    	}
    	
		if (!message.isEmpty()){
			new AlertDialog.Builder(this)
		    .setTitle(getResources().getString(R.string.failTitle))
		    .setMessage(message)
		    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
		        
		    	public void onClick(DialogInterface dialog, int which) { 
		        }
		    	
		     })
		    .setIcon(android.R.drawable.ic_dialog_alert).show();
			
		}
}
    
	public void menuModeSelected(View view){
		Intent myIntent = new Intent(view.getContext(), MenuActivity.class);
        startActivityForResult(myIntent, 0);
	}

	public void faceTrackingModeSelected(View view){
		Intent myIntent = new Intent(view.getContext(), CameraActivity.class);
        startActivityForResult(myIntent, 0);
	}

	public void onTestSucceed(){

		getFragmentManager().beginTransaction().add(R.id.container, new SelectionFragment())
        .setCustomAnimations(android.R.anim.slide_in_left,android.R.anim.slide_out_right).commit();
		
	}
	
	public void onTestFailed(){
		String message= getResources().getString(R.string.connectionError);
		
		new AlertDialog.Builder(this)
	    .setTitle(getResources().getString(R.string.failTitle))
	    .setMessage(message)
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        
	    	public void onClick(DialogInterface dialog, int which) { 
	        }
	    	
	     })
	    .setIcon(android.R.drawable.ic_dialog_alert).show();
	}
	
}
