package pl.edu.agh.toik.nao.android.interfaces;

public abstract class ServerModule {

	private String moduleName;
	
	public abstract void onRegister();
	
	public abstract void onPreExecute();
	
	public abstract void onExecute();
	
	public abstract void onPostExecute();
	
}
