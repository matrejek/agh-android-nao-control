package pl.edu.agh.toik.nao.android.activity;

import java.io.IOException;

import pl.edu.agh.toik.nao.android.R;
import pl.edu.agh.toik.nao.android.communication.CommunicationManager;
import android.graphics.PixelFormat;
import android.hardware.Camera;
import android.hardware.Camera.Face;
import android.hardware.Camera.FaceDetectionListener;
import android.os.Bundle;
import android.view.SurfaceHolder;
import android.view.SurfaceView;
import android.widget.TextView;

public class CameraActivity extends AbstractActivity {

	
	 private SurfaceView surfaceView;  
     private Camera camera;
     SurfaceHolder.Callback sh_ob = null;
     SurfaceHolder surface_holder        = null;
     SurfaceHolder.Callback sh_callback  = null;

     @Override
     protected void onCreate(Bundle savedInstanceState) {
         super.onCreate(savedInstanceState);
         setContentView(R.layout.activity_camera);
         getWindow().setFormat(PixelFormat.TRANSLUCENT);

         surfaceView = (SurfaceView) findViewById(R.id.surfaceView1); 
         if (surface_holder == null) {
             surface_holder = surfaceView.getHolder();
         }

         sh_callback = my_callback();
         surface_holder.addCallback(sh_callback);
         TextView label = ((TextView)findViewById(R.id.serverIP1));
         label.setText(label.getText().toString()+"  "+CommunicationManager.getInstance().getInfoString());
     }

         SurfaceHolder.Callback my_callback() {      
             SurfaceHolder.Callback ob1 = new SurfaceHolder.Callback() {

                 @Override
                 public void surfaceDestroyed(SurfaceHolder holder) {
                       camera.stopPreview();
                       camera.release();
                       camera = null;
                 }

                 @Override
                 public void surfaceCreated(SurfaceHolder holder) {
                     camera = openFrontFacingCameraGingerbread();//Camera.open();

                       try {
                            camera.setPreviewDisplay(holder);  
                       } catch (IOException exception) {  
                             camera.release();  
                             camera = null;  
                       }
                 }

                 @Override
                 public void surfaceChanged(SurfaceHolder holder, int format, int width,int height) {
                     camera.setDisplayOrientation(90);
;
                     camera.setFaceDetectionListener(new FaceDetectionListener() {
						
						@Override
						public void onFaceDetection(Face[] faces, Camera camera) {
							System.out.println("aa "+faces.length+" ");
							
						}
					});
                	 camera.startPreview();
                	 camera.startFaceDetection();
                 }
             };
             return ob1;
     }
         
     private Camera openFrontFacingCameraGingerbread() {
    	    int cameraCount = 0;
    	    Camera cam = null;
    	    Camera.CameraInfo cameraInfo = new Camera.CameraInfo();
    	    cameraCount = Camera.getNumberOfCameras();
    	    for (int camIdx = 0; camIdx < cameraCount; camIdx++) {
    	        Camera.getCameraInfo(camIdx, cameraInfo);
    	        if (cameraInfo.facing == Camera.CameraInfo.CAMERA_FACING_FRONT) {
    	            try {
    	                cam = Camera.open(camIdx);
    	            } catch (RuntimeException e) {
    	                
    	            }
    	        }
    	    }

    	    return cam;
    	}
	
     
     
}
