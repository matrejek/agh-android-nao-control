package pl.edu.agh.toik.nao.android;

import pl.edu.agh.toik.nao.android.interfaces.AbstractMobileModule;
import pl.edu.agh.toik.nao.android.interfaces.ModuleExecutor;
import android.app.Application;
import android.content.Context;
import android.content.pm.PackageManager.NameNotFoundException;

public class NaoApplication extends Application implements ModuleExecutor{

    private static Context context;
    
    private static NaoApplication instance;

    public void onCreate(){
        super.onCreate();
        NaoApplication.context = getApplicationContext();
    }

    public static Context getAppContext() {
        return NaoApplication.context;
    }
    
    public static String getVersion() throws NameNotFoundException{
    	return NaoApplication.getAppContext().getPackageManager()
	    .getPackageInfo(NaoApplication.getAppContext().getPackageName(), 0).versionName;
    }

	@Override
	public void execute(String moduleName, String parameters,AbstractMobileModule callbacks) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public ModuleExecutor getInstance() {
		return instance;
	}
}