package pl.edu.agh.toik.nao.android.communication;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapPrimitive;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

public final class CommunicationManager {

	private static CommunicationManager instance;

	private static String serverIP;
	private static int serverPort;

	private static String PROTOCOL = "http://";

	private static final String NAMESPACE = "http://hello_webservice/";
	private static final String TEST_METHOD = "hello";
	private static final String CALL_METHOD = "callModule";
	private static final String SOAP_ACTION = "http://hello_webservice/hello";

	private static final String ERROR_STRING = "MOD_ERROR";

	public static CommunicationManager getInstance() {
		if (instance == null) {
			instance = new CommunicationManager();
		}
		return instance;
	}

	public void init(String sIP, int sPort) {
		serverIP = sIP;
		serverPort = sPort;
	}

	public String getServerIP() {
		return serverIP;
	}

	public int getServerPort() {
		return serverPort;
	}

	public String getInfoString() {
		return serverIP + ":" + serverPort;
	}

	public String getURL() {
		String addr = "";
		addr = PROTOCOL + getInfoString() + "/SimplestHelloService";

		return addr;
	}

	public boolean performConnectionTest() {
		SoapObject request = new SoapObject(NAMESPACE, TEST_METHOD);

		PropertyInfo propInfo = new PropertyInfo();
		propInfo.name = "arg0";
		propInfo.type = PropertyInfo.STRING_CLASS;
		propInfo.setValue("abc");
		request.addProperty(propInfo);

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(getURL());

		try {
			androidHttpTransport.call(SOAP_ACTION, envelope);
			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope
					.getResponse();
			String res = resultsRequestSOAP.toString();
			return res.equalsIgnoreCase("OK");
		} catch (Exception e) {
			return false;
		}

	}

	public String invokeModule(String module, String variant, String params) {

		SoapObject request = new SoapObject(NAMESPACE, CALL_METHOD);
		PropertyInfo moduleProp = new PropertyInfo();
		moduleProp.name = "arg0";
		moduleProp.type = PropertyInfo.STRING_CLASS;
		moduleProp.setValue(module);
		request.addProperty(moduleProp);

		PropertyInfo variantProperty = new PropertyInfo();
		variantProperty.name = "arg1";
		variantProperty.type = PropertyInfo.STRING_CLASS;
		variantProperty.setValue(variant);
		request.addProperty(variantProperty);
		
		PropertyInfo paramsProperty=new PropertyInfo();
		paramsProperty.name="arg2";
		paramsProperty.type=PropertyInfo.STRING_CLASS;
		paramsProperty.setValue(params);
		request.addProperty(paramsProperty);
		

		SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(
				SoapEnvelope.VER11);
		envelope.setOutputSoapObject(request);

		HttpTransportSE androidHttpTransport = new HttpTransportSE(getURL());

		try {
			androidHttpTransport.call("http://hello_webservice/callModule", envelope);
			
			SoapPrimitive resultsRequestSOAP = (SoapPrimitive) envelope.getResponse();
			try{
				String res = (String) resultsRequestSOAP.getValue();
			
				return res;
			
			}catch(Exception e){
				e.printStackTrace();
			}
		} catch (Exception e) {
			e.printStackTrace();
			return ERROR_STRING;
		}
		return "";

	}

}
