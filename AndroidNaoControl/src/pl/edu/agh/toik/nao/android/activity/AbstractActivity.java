package pl.edu.agh.toik.nao.android.activity;

import pl.edu.agh.toik.nao.android.NaoApplication;
import pl.edu.agh.toik.nao.android.R;
import android.app.Activity;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.pm.PackageManager.NameNotFoundException;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;

public abstract class AbstractActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
        case R.id.info:
            displayInfo();
            return true;
        default:
            return super.onOptionsItemSelected(item);
        }
    }

	private void displayInfo() {
		
		String versionName="";
		try {
			versionName = NaoApplication.getVersion();
		} catch (NameNotFoundException e) {
			e.printStackTrace();
		}
		
		new AlertDialog.Builder(this)
	    .setTitle("Android Nao Control")
	    
	    .setMessage("Current version: "+versionName+"\n\nFor help and more info visit project site at Bitbucket.org\n\nAplication by Mateusz Matrejek")
	    .setPositiveButton("OK", new DialogInterface.OnClickListener() {
	        
	    	public void onClick(DialogInterface dialog, int which) { 
	        }
	    	
	     })
	    .setIcon(android.R.drawable.ic_dialog_info).show();
		
	}
	
	
}
