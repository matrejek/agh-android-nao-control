![Untitled-2.png](https://bitbucket.org/repo/9Myz8X/images/3366686138-Untitled-2.png)

# O projekcie

Sterowanie robotem mobilnym można realizować na wiele sposobów. Możliwe jest podejście uwzględniające oprogramowanie robota i zapewnienie mu pewnej dozy autonomii. Nie zawsze jednak jest to pożądane ze względu na uwarunkowania otoczenia uwzględniające konieczność reakcji na zdarzenia nie przewidziane w procesie analizy tworzonego oprogramowania. Problemem jest zapewnienie takiego sposobu obsługi, który gwarantuje niezależność położenia robota oraz operatora, a także angażuje możliwie nieduże środki umożliwiając przy tym sprawną komunikację na linii robot - operator. 

Prezentowany projekt ma za zadanie dostarczyć rozwiązanie umożliwiające sterowanie humanoidalnym robotem Nao tworzonym przez francuską firmę Aldebaran Robotics. Proponowane rozwiązanie wykorzystywać będzie urządzenie mobilne z systemem Android.

Projekt realizowany jest w ramach zajęć z przedmiotu Technologie Obiektowe i Komponentowe w roku akademickim 2013/4.