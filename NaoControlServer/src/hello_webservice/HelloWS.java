package hello_webservice;
import javax.jws.WebMethod;
import javax.jws.WebService;


@WebService(name = "HelloWS", targetNamespace = "http://hello_webservice/")

public interface HelloWS {
	@WebMethod(operationName = "hello")
	public String hello(String name);
}