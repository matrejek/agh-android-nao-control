package pl.edu.agh.toik.nao.server;

import hello_webservice.HelloWSImpl;

import java.awt.EventQueue;

import javax.xml.ws.Endpoint;

public class Application {

	private static Application app;
	private static ServerWindow window;
	public static void main(String[] args) {
	
		app = new Application();
	
		//start GUI
		
		EventQueue.invokeLater(new Runnable() {


			public void run() {
				try {
					window = new ServerWindow();
					window.frmNaoControlServer.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
		
		
		
		
	}
	
	public void startMobileEndpoint(String serverIP, String serverPort){
		try{
			Endpoint.publish("http://"+serverIP+":"+serverPort+"/SimplestHelloService?wsdl", new HelloWSImpl());
			window.onListenerStarted();
			logMessage("Mobile endpoint started",false);
			logMessage("Listening for commands at http://"+serverIP+":"+serverPort+"/SimplestHelloService?wsdl",false);
		}catch(Exception e){
			logMessage("Mobile endpoint unable to start: "+e.getMessage(),false);
			e.printStackTrace();
		}
	}



	public static Application getApp() {
		return app;
	}
	
	public void logMessage(String msg, boolean isCommand){
		if(isCommand){
			window.logCommand(msg);
		}else{
			window.logMessage(msg);
		}

	}

	
	
}
