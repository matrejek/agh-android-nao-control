package pl.edu.agh.toik.nao.server;

import java.awt.BorderLayout;
import java.awt.Frame;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.regex.Pattern;

import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JSeparator;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;

public class ServerWindow {

	JFrame frmNaoControlServer;
	private JTextField textField;
	private JTextField textField_1;
	private JTextField endpointAddrField;
	private JTextField endpointPortField;
	private JTextArea textArea;
	private JButton listenerButton;
	private JLabel endpointStatusLabel;
	private JLabel endpointAddrLabel;
	private JLabel endpointPortLabel;

	private JCheckBox commandLoggingBox;
	private JPanel endpointPanel;

	private static final String IPADDRESS_PATTERN = 
			"^([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])\\." +
			"([01]?\\d\\d?|2[0-4]\\d|25[0-5])$";
	
	private static final String DEFAULT_ENDPOINT_ADDR = "127.0.0.1";
	private static final String DEFAULT_ENDPOINT_PORT = "8383";

	public ServerWindow() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmNaoControlServer = new JFrame();
		frmNaoControlServer.setTitle("Nao Control Server");
		frmNaoControlServer.setBounds(100, 100, 530, 375);
		frmNaoControlServer.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		frmNaoControlServer.getContentPane().setLayout(new BorderLayout(0, 0));
		
		JPanel panel_7 = new JPanel();
		frmNaoControlServer.getContentPane().add(panel_7, BorderLayout.SOUTH);
		panel_7.setLayout(new GridLayout(1, 4, 0, 0));
		
		JLabel lblNewLabel_4 = new JLabel("Nao connection:");
		panel_7.add(lblNewLabel_4);
		
		JLabel lblNewLabel_5 = new JLabel("Disconnected");
		panel_7.add(lblNewLabel_5);
		
		JSeparator separator = new JSeparator();
		separator.setOrientation(SwingConstants.VERTICAL);
		panel_7.add(separator);
		
		JLabel lblNewLabel_6 = new JLabel("Endpoint status:");
		panel_7.add(lblNewLabel_6);
		
		endpointStatusLabel = new JLabel("Disabled");
		panel_7.add(endpointStatusLabel);
		
		JPanel panel = new JPanel();
		panel.setBorder(new TitledBorder(UIManager.getBorder("TitledBorder.border"), "Configuration", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frmNaoControlServer.getContentPane().add(panel, BorderLayout.NORTH);
		panel.setLayout(new GridLayout(1, 2, 0, 0));
		
		JPanel panel_2 = new JPanel();
		panel_2.setBorder(new TitledBorder(null, "Nao Connection", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(panel_2);
		panel_2.setLayout(new GridLayout(2, 2, 0, 5));
		
		JLabel lblNewLabel_1 = new JLabel("Robot IP");
		panel_2.add(lblNewLabel_1);
		
		textField = new JTextField();
		panel_2.add(textField);
		textField.setColumns(10);
		
		JLabel lblNewLabel = new JLabel("Robot port");
		panel_2.add(lblNewLabel);
		
		textField_1 = new JTextField();
		panel_2.add(textField_1);
		textField_1.setColumns(10);
		
		endpointPanel = new JPanel();
		endpointPanel.setBorder(new TitledBorder(null, "Endpoint", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel.add(endpointPanel);
		endpointPanel.setLayout(new GridLayout(2, 2, 0, 5));
		
		endpointAddrLabel = new JLabel("Endpoint IP");
		endpointPanel.add(endpointAddrLabel);
		
		endpointAddrField = new JTextField();
		endpointAddrField.setText(DEFAULT_ENDPOINT_ADDR);
		
		endpointPanel.add(endpointAddrField);
		endpointAddrField.setColumns(10);
		
		endpointPortLabel = new JLabel("Endpoint port");
		endpointPanel.add(endpointPortLabel);
		
		endpointPortField = new JTextField();
		endpointPortField.setText(DEFAULT_ENDPOINT_PORT);
		endpointPanel.add(endpointPortField);
		endpointPortField.setColumns(10);
		
		JPanel panel_1 = new JPanel();
		frmNaoControlServer.getContentPane().add(panel_1, BorderLayout.WEST);
		panel_1.setLayout(new GridLayout(2, 1, 0, 0));
		
		JPanel panel_4 = new JPanel();
		panel_4.setBorder(new TitledBorder(null, "Actions", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_4);
		panel_4.setLayout(new GridLayout(0, 1, 0, 0));
		
		JButton btnNewButton = new JButton("Connect NAO");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
			}
		});
		panel_4.add(btnNewButton);
		
		listenerButton = new JButton("Start listener");
		
		listenerButton.addActionListener(new ActionListener() {
			
			@Override
			public void actionPerformed(ActionEvent arg0) {
				
				
		    	String message = "";
		    	String serverIP = endpointAddrField.getText();
		    	String serverPort = endpointPortField.getText();
		    	
		    	 if(serverIP.equalsIgnoreCase("") || serverPort.equalsIgnoreCase("")){

		    		message = "Please fill endpoint configuration in order to continue.";
		    		
		    	}else if ( !Pattern.compile(IPADDRESS_PATTERN).matcher(serverIP).matches()){
		    		message = "Please provide vaild IP address in order to continue.";
		    	}else{

		    		Application.getApp().startMobileEndpoint(serverIP,serverPort);
		    	}
				
		    	 
		    	 if (!message.isEmpty()){
		    		 JOptionPane.showMessageDialog(new Frame(), message);
		    	 }
			}
		});
		
		panel_4.add(listenerButton);
		
		JPanel panel_5 = new JPanel();
		panel_5.setBorder(new TitledBorder(null, "Logging", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		panel_1.add(panel_5);
		panel_5.setLayout(new GridLayout(3, 1, 0, 0));
		
		JCheckBox chckbxNewCheckBox = new JCheckBox("Log to file");
		panel_5.add(chckbxNewCheckBox);
		
		commandLoggingBox = new JCheckBox("Log commands");
		commandLoggingBox.setSelected(true);
		panel_5.add(commandLoggingBox);
		
		JPanel panel_6 = new JPanel();
		panel_6.setBorder(new TitledBorder(null, "Messages", TitledBorder.LEADING, TitledBorder.TOP, null, null));
		frmNaoControlServer.getContentPane().add(panel_6, BorderLayout.CENTER);
		panel_6.setLayout(new BorderLayout(0, 0));
		
		textArea = new JTextArea();
		textArea.setLineWrap(true);
		panel_6.add(textArea, BorderLayout.CENTER);
		
	}
	
	public void logCommand(String msg){
		if (commandLoggingBox.isSelected()){
			logMessage(msg);
		}
	}
	
	public void logMessage(String msg){
		DateFormat df = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");
		Date today = Calendar.getInstance().getTime();        
		String logDate = df.format(today);
		msg = logDate+" :: "+msg+"\n";
		textArea.setText(textArea.getText()+msg);
	}
	
	public void onListenerStarted(){
		listenerButton.setEnabled(false);
		endpointStatusLabel.setText("Listening");
		endpointAddrField.setEnabled(false);
		endpointAddrLabel.setEnabled(false);
		endpointPortField.setEnabled(false);
		endpointPortLabel.setEnabled(false);
		endpointPanel.setEnabled(false);
	}
	

}
